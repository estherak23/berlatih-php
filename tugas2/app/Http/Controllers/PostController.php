<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\question;



class PostController extends Controller
{
    public function create(){


return view('post.create');


    }



    public function store(Request $request){


$request->validate([

    'judul'=> 'required|:posts',
    'isi'=> 'required'


]) ;
      
//model eloquent nambah atau create
//$question= new question;
//$question->judul=$request["judul"];
//$question->isi=$request["isi"];
//$question->save();


//mass asignment secara masiive nammbah data




$question = question::create([
"judul"=>$request["judul"],
"isi"=>$request["isi"]


]);





      //  $query=DB::table('questions')->insert([
        //    
          //  "judul"=> $request["judul"],
           // "isi"=> $request["isi"]

// ]);
return redirect('/posts')->with('success','post berhasil disimpan');
        
            }



            public function index(){

                  //  $post=DB:: table('questions')->get();
                  
                  //secara model eloquent menampilkan
                $post= question::all();


                return view('post.index',compact('post'));
            }









            public function show($id){
               // $post=DB :: table('questions')->where('id',$id)->first();

//secara model eloquent show
                $post= question::find($id);

                return view ('post.show',compact('post'));
            }
            



            public function edit($id){
               // $post=DB :: table('questions')->where('id',$id);
                

//secara model eloquent edit
                $post= question::find($id);


                return view ('post.edit',compact('post'));
            }
            




            public function update($id,request $request){

              //  $query=DB :: table('questions')
                //->where('id',$id)
                //->update([
                 //"judul"=> $request["judul"],
                //"isi"=> $request["isi"]

//]);

//$request->validate([

  //  'judul'=> 'required|:posts',
    //'isi'=> 'required']);


$update=question::where('id',$id)->update([


    "judul"=>$request["judul"],
    "isi"=>$request["isi"]


]);
                
                
return redirect('/posts')->with('success','berhasil update');
            }


            public function destroy($id){

                //$query=DB :: table('questions') ->where('id',$id)->delete();



                question::destroy($id);

                return redirect('/posts')->with('success','berhasil dihapus');

            }


}
