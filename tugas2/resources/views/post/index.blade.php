@extends('adminlte.master')

@section('content')



<div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">pertanyaan Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @if(session('success'))
            <div class='alert alert-success'>

{{session('success')}}

            </div>
            @endif
            <a class="btn btn-info" href="/posts/create">create new</a>
              <table class="table table-bordered">
                <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>judul</th>
                  <th>isi</th>
                  <th style="width: 40px">Action</th>
                </tr>
                </thead>
               <tbody>
                  @foreach($post as $key => $post)
                  <tr>
                  
                  <td>{{$key +1 }}</td>
                  <td>{{$post ->judul}}</td>
                  <td>{{$post ->isi}}</td>
                  <td style="display: flex;">
                  <a href="/posts/{{$post->id}}/show" class="btn btn-info btn-sm">show</a>
                  <a href="/posts/{{$post->id}}/edit" class="btn btn-info btn-sm">edit</a>

<form action="/posts/{{$post->id}}" method="post">
@csrf
@method('DELETE')
<input type="submit" value="delete" class="btn btn-danger btn-sm">
</form>


                  </td>
                  </tr>
                 
                  
                @endforeach
              </tbody>
              </table>
            </div>
            <!-- /.card-body -->
           







@endsection