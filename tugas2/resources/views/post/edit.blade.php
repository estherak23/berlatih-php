@extends('adminlte.master')

@section('content')
<div class="card card-primary">
            <div class="card-header with-border">
              <h3 class="card-title">edit{{$post->id}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" action="/posts/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
              <div class="card-body">
                <div class="form-group">
                  <label for="title"><title>judul</title></label>
                  <input type="text" class="form-control" id="judul" value=" {{old('judul',$post->judul)}}"name="judul" placeholder="Enter judul">


                    @error('judul')
                  <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="form-group">
                  <label for="body">body</label>
                  <input type="text" class="form-control" id="isi" value=" {{old('isi',$post->isi)}}" name="isi" placeholder="isi">


                  @error('isi')
                  <div class="alert alert-danger">{{ $message }}</div>
                    @enderror



                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">create</button>
              </div>
            </form>
          </div>
@endsection