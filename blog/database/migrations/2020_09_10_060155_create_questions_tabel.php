<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_tabel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('isi');
            $table->string('judul');
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diperbaharui');
            $table->unsignedBigInteger('jawaban_tepat_id');

            $table->foreign('jawaban_tepat_id')->references('id')->on('answers');

            $table->unsignedBigInteger('profile_id');

    $table->foreign('profile_id')->references('id')->on('profile');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_tabel');
    }
}
