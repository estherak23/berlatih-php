<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php 

class Animal {


  
    public $animalname;

    private $legs = 2;
    public function legs()
    {
      echo $this->legs."<br>";
    }


    private $darahdingin = "false";
    public function darahdingin()
    {
      echo $this->darahdingin . "<br>"."<br>";
    }





    public function __construct($animalname){
    $this->animalname= $animalname;
}


}

$shaun = new Animal("shaun");
echo $shaun->animalname. "<br>" ;
$shaun->legs();
$shaun->darahdingin();




class Ape extends Animal {
    private $yell = "Auooo";
    public function yell()
    {
      echo $this->yell . "<br>"."<br>";
    }
  }

  $sungokong = new Ape("kera sakti");
  echo $sungokong->animalname . "<br>";
  $sungokong->yell();



  class Frog extends Animal {
    private $jump = "hop hop";
    public function jump()
    {
      echo $this->jump . "<br>"."<br>";
    }
  }

  $kodok = new Frog("buduk");
  echo $kodok->animalname  . "<br>";
  $kodok->jump();

?>
</body>
</html>